TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    User.cpp \
    WechatServer.cpp

HEADERS += \
    WechatServer.h \
    Group.h \
    User.h \
    Util.h

