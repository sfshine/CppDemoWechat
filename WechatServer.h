#ifndef _WechatServer_H
#define _WechatServer_H

#include <iostream>
#include <vector>
#include "User.h"
#include "Group.h"
using namespace std;

class WechatServer{
    private:
        WechatServer(){}
        static WechatServer* m_Instance;
        vector<User*> mUsers;
        vector<Group*> mGroups;
    public:

        //need use singleton
        static WechatServer* GetInstance()
        {
            if(!m_Instance)
                m_Instance = new WechatServer();
            return m_Instance;
        }
        User* findUserById(int id);
        vector<User*> getUsers();
        void createGroup(vector<User*> users , int creatorId);
        void addUser(User* user);
};
#endif
