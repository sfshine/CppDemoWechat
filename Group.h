#ifndef _Group_H
#define _Group_H
//防止嵌套include导致编译死循环

#include <iostream>
#include <vector>
//#include "User.h"
using namespace std;
class User;//这里是C++类的前向声明，没有用include“User.h”
           //编译到一块之后User*自然指向了User class
            //其实很简单, 是先编译了Group又编译了User, 最后编译了WechatServer, 按照字母顺序
            //所以User也可以前向声明一个class WechatServer
class Group{
public:
    int mGroupId,mCreatorId;
    vector<User*> mMembers;
    Group(int groupId , int creatorId){
      mGroupId = groupId;
      mCreatorId = creatorId;
    }
    void addMembers(vector<User*> users){
        for (vector<User*>::size_type i = 0; i != users.size(); ++i)
        {
            mMembers.push_back(users[i]);
        }
    }

};

#endif
