#ifndef UTIL_H
#define UTIL_H
#include <iostream>
#include <string>
#include <sstream>
class Util{
  //如何连接string和int等, 流操作
  public:
    static string join(string stringValue, int intValue)
    {

        stringstream ss;
        ss << stringValue << intValue;
        return ss.str();
    }
};

#endif // UTIL_H
