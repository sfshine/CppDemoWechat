#ifndef _User_H
#define _User_H

#include <iostream>
#include <vector>
#include "Group.h"
#include "WechatServer.h"
using namespace std;

class User{
private:
    int mWeChatId, mQQ;
    string mPhone;
    string mEmail;
    vector<User*> mFriends;
    vector<string> mPhoneBook;
public:
    User(int weChatId ,int qq ,string phone , string email){
     mWeChatId = weChatId;
     mQQ = qq;
     mPhone = phone;
     mEmail = email;
    }

    vector<User*> getFriends(){
      return mFriends;
    }
    void addPhone(string phone){
      mPhoneBook.push_back(phone);
    }
    void addFriend(User* user ){
      mFriends.push_back(user);
    }

    string getEmail(){
        return mEmail;
    }
    int getId(){
        return mWeChatId;
    }

    void createGroup();
    void sendRecomend();
};
#endif
