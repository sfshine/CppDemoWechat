#include <iostream>
#include <vector>
#include <stdlib.h>
#include "User.h"
#include "WechatServer.h"
#include "Group.h"
#include "Util.h"
#include "User.h"

using namespace std;

void addFreind(User* user ,vector<User*> userlist){
    int friendCount = 0;
    for (vector<User*>::size_type i = 0; i != userlist.size(); ++i)
    {
        User* myfriend = userlist[i];
        //can not add myself
        if(myfriend->getId() != user->getId() && friendCount < 5){
            cout << "adding friend : "<< myfriend->getId()<<endl;
            user->addFriend(myfriend);
            friendCount ++;
        }
    }
}


int main()
{
    WechatServer* wechatServer = WechatServer ::GetInstance();

    //regist user
    for(int i = 0 ; i < 8 ; i++){
        string email = Util::join("sfshine" ,i) + "@qq.com";
        string phone = Util::join("1566622007",i);
        //User user(1000 + i, 84508241+ i , phone , email);
        //cout<<&user; 这样放到了栈中, 这里虽然循环了八次, 但是打印出来的地址其实都是同一个
        //需要把user放到堆中, 这样才可以生成八个User
        User* user = new User(1000 + i, 84508241+ i , phone , email);
        cout << user;
        wechatServer->addUser(user);
    }

    vector<User*> userlist = wechatServer->getUsers();
    User* user1 = wechatServer->findUserById(1000);
    User* user2 = wechatServer->findUserById(1001);

    addFreind(user1 ,userlist);
    addFreind(user2 ,userlist);

    user1->createGroup();
    user2->createGroup();

    for(int i = 0 ; i < 20; i++){
        string phone;
        if(i < 10){
             phone = Util::join("1566685564",i);
        }else {
             phone = Util::join("156668554",i);
        }
        user1->addPhone(phone);
    }
    user1->sendRecomend();

}



